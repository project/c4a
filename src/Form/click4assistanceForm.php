<?php
/**
 * @file
 * Contains Drupal\click4assistance\Form\MessagesForm.
 */
namespace Drupal\click4assistance\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class click4assistanceForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'click4assistance.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'click4assistance_form';
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('click4assistance.adminsettings');

 
 $form['text_header'] = array
      (
        '#prefix' => '<h2>',
        '#suffix' => '</h2>',
        '#markup' => t('Please enter your Click4Assistance account details below.'),
        '#weight' => -100,
      );

     $form['text_description'] = array
      (
        '#prefix' => '<h4>',
        '#suffix' => '</h4>',
        '#markup' => t('If you do not have a Click4Assistance account, <a href="https://www.click4assistance.co.uk/live-chat-software-free-trial" target="_blank">click here for your free trial.</a>'),
      
      ); 
     $form['guid'] = array(
      '#type' => 'textfield',
      '#title' => t('Account Guid:'),
      '#required' => TRUE,
      '#default_value' => $config->get('guid'),
    );
    $form['script_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Script ID'),
      '#required' => TRUE,
      '#default_value' => $config->get('script_id'),
    );

    $form['optionl_script'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Optional Script'),
      '#default_value' => $config->get('optionl_script'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    //file_put_contents('../../click4assistance.js', 'gdgfdgfddfgdg');\

   
    $this->config('click4assistance.adminsettings')
      ->set('optionl_script', $form_state->getValue('optionl_script'))
      ->set('guid', $form_state->getValue('guid'))
      ->set('script_id', $form_state->getValue('script_id'))
      ->save();


 $script = 'function InitialiseC4A() {var Tool1 = new C4A.Tools(1);C4A.Run("'. $form_state->getValue('guid').'");}';
    file_put_contents('./modules/c4a/click4assistance.js', $script);


  }
}



